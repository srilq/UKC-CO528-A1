// login: msj4
// surname: Jones

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

abstract class Main {

    // CONFIG CONSTS
    static final int CONFIG_WIDTH = 4;
    static final int CONFIG_HEIGHT = 4;
    static final int CONFIG_LENGTH = CONFIG_WIDTH * CONFIG_HEIGHT;
    static final char CONFIG_BLANK_CHAR = '_';
    static final char CONFIG_IMMOVABLE_CHAR = '+';

    // PROGRAM ARGS
    static boolean silent = false;
    static boolean verbose = false;

    /**
     * Handles program arguments. Returns false if the program should stop.
     * @param args Program arguments given as array of Strings
     * @return boolean Whether the program should continue to run.
     */
    static boolean handleArgs(String[] args)
    {
        // handle args
        boolean helpMode = false;
        if (args.length > 0) {
            List<String> argsList = Arrays.asList(args);
            silent = argsList.contains("--silent") || argsList.contains("-s");
            verbose = !silent && argsList.contains("--verbose") || argsList.contains("-v");
            helpMode = argsList.contains("--help") || argsList.contains("-h");
        }

        if (!helpMode) println("Add \'--help\' or \'-h\' for args info.");
        else {
            System.out.println("--verbose -v   Additional information in console output.");
            System.out.println("--silent  -s   Silent running.");
            if (verbose) System.out.println();
            return false;
        }

        if (verbose) println("Verbose output mode.");

        return true;
    }

    /**
     * Print puzzle solution string.
     * @param solution Solution to print given as concatenated configuration Strings
     */
    static void printSolution(String solution)
    {
        String divider = "";
        for (int i = 0; i < (solution.length() / CONFIG_HEIGHT) + (solution.length() / CONFIG_LENGTH) - 1; i ++)
        {
            divider += (i + 1) % (CONFIG_WIDTH + 1) == 0 ? "|" : "-";
        }

        println("Solution:");
        println(divider);
        println(solutionToString(solution));
        println(divider);
    }

    /**
     * Write puzzle solution string to file in the working directory.
     * @param filename File name including extension
     * @param solution Solution to write given as concatenated configuration Strings
     */
    static void writeSolutionToFile(String filename, String solution)
    {
        if(verbose) println("Writing solution to file \"" + filename + "\" ...");
        try
        {
            FileWriter writer = new FileWriter(filename);
            PrintWriter printWriter = new PrintWriter(writer);
            printWriter.printf("%s", solutionToString(solution));
            printWriter.close();
            println("File written.");
        }
        catch(IOException e)
        {
            handleException(e);
        }
    }

    /**
     * Returns given puzzle solution string with pretty formatting.
     * @param solution Solution given as concatenated configuration Strings
     * @return Pretty solution String
     */
    static String solutionToString(String solution)
    {
        String out = "";

        int configs = solution.length() / CONFIG_LENGTH;
        for (int y = 0; y < CONFIG_HEIGHT; y++)
        {
            for (int c = 0; c < configs; c++)
            {
                int first = (CONFIG_LENGTH * c) + (CONFIG_WIDTH * y);
                out += solution.substring(first, first + CONFIG_WIDTH);
                if (c != configs - 1) out += " ";
            }
            if (y != CONFIG_HEIGHT - 1) out += "\n";
        }

        return out;
    }

    /**
     * Prints the given string to System.out if not in silent mode.
     * @param string String
     */
    static void print(String string)
    {
        if (!silent) System.out.print(string);
    }

    /**
     * Prints the given string with appended newline to System.out if not in silent mode.
     * @param string String
     */
    static void println(String string)
    {
        print(string + "\n");
    }
    /**
     * Prints a newline character to System.out if not in silent mode.
     */
    static void println()
    {
        print("\n");
    }

    /**
     * Do something appropriate with the given exception.
     * @param e Exception
     */
    static void handleException(Exception e)
    {
        System.out.println(e.getMessage());
    }

    /**
     * Returns given string with characters at given indices swapped.
     * @param string String
     * @param i Index of first character
     * @param j Index of second character
     * @return String with characters swapped
     */
    static String swapChars(String string, int i, int j)
    {
        char[] chars =  string.toCharArray();
        char temp = chars[i];
        chars[i] = chars[j];
        chars[j] = temp;
        return new String(chars);
    }

    /**
     * Concatenates the given list of strings into a single string.
     * @param strings The given List<String>
     * @return Each String concatenated into a single String
     */
    static String concatList(List<String> strings)
    {
        String out = "";
        for (String s : strings)
            out += s;
        return out;
    }
}
