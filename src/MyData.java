// login: msj4
// surname: Jones

class MyData {

    static final String[] PUZZLES = {
            "+ad++bdacd_bddb+2+ad++bda_cbbddd+",
            "+dd++adbcbdab_d+2+db++adddcab_bd+",
            "+db++dacabdbd_d+2+db++bacd_dddba+",
            "+ba++ddbbdc_dad+2+ba++cdddbdba_d+",
            "+ab++bddbd_acdd+2+db++ab_dcdabdd+",
            "+da++_dbdddcbba+2+dd++a_bdddcbba+",
            "+db++d_dcabdbad+2+ad++ddbbcdba_d+",
            "+bd++db_addbdac+2+_b++bddcdabadd+",
            "+cb++ddaddb_bda+2+bd++c_adddbbda+",
            "+ba++adcbd_dbdd+2+a_++bacbdddbdd+",
            "+ba++ddbd_ddbac+2+ba++ddbb_ddacd+",
            "+dd++badb_adbcd+2+db++dddcaa_bbd+",
            "+ab++c_dddddabb+2+bc++bad_dbdadd+",
            "+bd++badc_dddab+2+cb++dddbd_adab+",
            "+dd++dbadbcb_da+2+dd++cbb_bddada+",
            "+db++abd_bdaddc+2+db++_adbdbaddc+"
    };

}
