// login: msj4
// surname: Jones

import java.util.LinkedList;

class DepthFirstMain extends Main {

    /**
     * Program entry point.
     * @param args Program arguments given as array of Strings
     */
    public static void main(String[] args)
    {
        boolean cont = handleArgs(args);
        if (!cont) return;

        exec(MyData.PUZZLES);
    }

    /**
     * Execute solver for an array of puzzle strings.
     * @param puzzles Puzzle strings given as array of Strings
     */
    private static void exec(String[] puzzles)
    {
        println("Executing for " + puzzles.length + " puzzles ...");

        String[] solutions = new String[puzzles.length];

        for (int i = 0; i < puzzles.length; i++)
        {
            if (verbose) println();

            solutions[i] = solve(puzzles[i]);

            if (verbose) printSolution(solutions[i]);

            String filename = puzzles[i] + ".txt";
            writeSolutionToFile(filename, solutions[i]);
        }
        if (verbose) println();
    }

    /**
     * Solve given puzzle string and return solution route.
     * @param puzzle Puzzle given as start configuration String and destination configuration String separated by a '2'
     * @return Solution route given as concatenated configuration Strings
     */
    private static String solve(String puzzle)
    {
        String start = puzzle.substring(0, CONFIG_LENGTH);
        String dest = puzzle.substring(puzzle.length()- CONFIG_LENGTH);
        long startTime = System.nanoTime();

        println("Solving \"" + puzzle + "\" ...");
        if (verbose) println("  Start: \"" + start + "\" ...");
        if (verbose) println("  Dest:  \"" + dest + "\" ...");
        if (verbose) println("  Running depth-first search ...");
        int depth = 1;
        while (true)
        {
            String solution = depthFirstSearch(start, dest, depth);
            if (solution != null) {
                if (verbose) println("  Finished depth-first search.");
                println("Solved! (" + ((System.nanoTime() - startTime) / 1000000L) + " ms)");
                return solution;
            }
            depth++;
            if (verbose) println("    We have to go deeper (" + depth + ") ...");
        }
    }

    /**
     * Recursive depth-first search algorithm returns the optimal valid route from the given start configuration to the
     * given destination configuration.
     * Implements "deja vu", ensuring the same configuration is never searched twice.
     * @param route Previously-searched configurations given as a concatenated list of configuration Strings
     * @param start Start configuration String
     * @param dest Destination configuration String
     * @param depth Depth of the search
     * @return Solution route given as concatenated configuration Strings
     */
    private static String depthFirstSearch(LinkedList<String> route, String start, String dest, int depth)
    {
        // NOTE: method based on code shown in lectures by Andy King: https://moodle.kent.ac.uk/2016/pluginfile.php/956/course/section/58882/co884-lecture1-handouts.pdf

        if (depth == 0) return null;

        if (start.equals(dest)) return start;
        else {
            String[] nextConfigs = getNextConfigs(start);
            for (String nextConfig : nextConfigs)
            {
                if (nextConfig == null || route.contains(nextConfig)) continue;

                LinkedList<String> routeCpy = (LinkedList<String>) route.clone();
                routeCpy.add(nextConfig);

                String solution = depthFirstSearch(routeCpy, nextConfig, dest, depth - 1);
                if (solution != null) return start + solution;
            }
            return null;
        }
    }
    /**
     * Recursive depth-first search algorithm returns the optimal valid route from the given start configuration to the
     * given destination configuration.
     * Implements "deja vu", ensuring the same configuration is never searched twice.
     * @param start Start configuration String
     * @param dest Destination configuration String
     * @param depth Depth of the search
     * @return Solution route given as concatenated configuration Strings
     */
    private static String depthFirstSearch(String start, String dest, int depth)
    {
        LinkedList<String> route = new LinkedList<>();
        route.add(start);
        return depthFirstSearch(route, start, dest, depth);
    }

    /**
     * For the given configuration, returns all the possible configurations after one move.
     * A.K.A. "next_configs"
     * @param config Configuration String
     * @return Next configurations given as array of configuration Strings
     */
    private static String[] getNextConfigs(String config)
    {
        int blank = config.indexOf(CONFIG_BLANK_CHAR);
        Direction[] directions = Direction.values();

        String[] nextConfigs = new String[directions.length];
        for (int i = 0; i < nextConfigs.length; i++)
        {
            if ((directions[i] == Direction.RIGHT && blank % CONFIG_WIDTH == CONFIG_WIDTH - 1)
                    || (directions[i] == Direction.LEFT && blank % CONFIG_WIDTH == 0)) {
                nextConfigs[i] = null;
            } else {
                int move;
                switch (directions[i]) {
                    case UP: move = blank - CONFIG_WIDTH; break;
                    case DOWN: move = blank + CONFIG_WIDTH; break;
                    case LEFT: move = blank - 1; break;
                    case RIGHT: move = blank + 1; break;
                    default: move = -1;
                }

                if (move < 0 || move >= config.length() || config.charAt(move) == CONFIG_IMMOVABLE_CHAR) {
                    nextConfigs[i] = null;
                } else nextConfigs[i] = swapChars(config, blank, move);
            }
        }

        return nextConfigs;
    }
}
