// login: msj4
// surname: Jones

import java.util.PriorityQueue;
import java.util.List;
import java.util.LinkedList;

/**
 * A puzzle configuration, its queue priority and route (previous) configurations.
 */
class Config implements Comparable<Config> {

    private int priority;
    private LinkedList<String> route;

    /**
     * Constructor for Config.
     * @param priority The priority
     * @param route The route
     */
    Config(int priority, LinkedList<String> route)
    {
        this.priority = priority;
        this.route = route;
    }
    /**
     * Constructor for Config.
     * @param priority The priority
     * @param string The configuration String
     */
    Config(int priority, String string)
    {
        this.priority = priority;
        this.route = new LinkedList<String>();
        route.add(string);
    }
    /**
     * Constructor for Config.
     * @param string The configuration String
     */
    Config(String string)
    {
        this.priority = -1;
        this.route = new LinkedList<String>();
        route.add(string);
    }

    /**
     * Compares the given Config with this Config and returns the difference using diff.
     * @param other The given Config
     * @return Number of tiles out of place
     */
    public int compareTo(Config other)
    {
        return priority - other.getPriority();
    }

    /**
     * Getter for priority field.
     * @return int priority
     */
    public int getPriority() { return priority; }

    /**
     * Getter for route field.
     * @return LinkedList<Config> route
     */
    public LinkedList<String> getRoute() { return route; }

    /**
     * Getter for configString field.
     * @return String configString
     */
    public String getString() { return route.getLast(); }
}

class AStarMain extends Main {

    /**
     * Program entry point.
     * @param args Program arguments given as array of Strings
     */
    public static void main(String[] args)
    {
        boolean cont = handleArgs(args);
        if (!cont) return;

        exec(MyData.PUZZLES);
    }

    /**
     * Execute solver for an array of puzzle strings.
     * @param puzzles Puzzle strings given as array of Strings
     */
    private static void exec(String[] puzzles)
    {
        println("Executing for " + puzzles.length + " puzzles ...");

        String[] solutions = new String[puzzles.length];

        for (int i = 0; i < puzzles.length; i++)
        {
            if (verbose) println();

            solutions[i] = solve(puzzles[i]);

            if (verbose) printSolution(solutions[i]);

            String filename = puzzles[i] + ".txt";
            writeSolutionToFile(filename, solutions[i]);
        }
        if (verbose) println();
    }

    /**
     * Solve given puzzle string and return solution route.
     * @param puzzle Puzzle given as start configuration String and destination configuration String separated by a '2'
     * @return Solution route given as concatenated configuration Strings
     */
    private static String solve(String puzzle)
    {
        String start = puzzle.substring(0, CONFIG_LENGTH);
        String dest = puzzle.substring(puzzle.length()- CONFIG_LENGTH);
        long startTime = System.nanoTime();

        println("Solving \"" + puzzle + "\" ...");
        if (verbose) println("  Start: \"" + start + "\" ...");
        if (verbose) println("  Dest:  \"" + dest + "\" ...");
        if (verbose) println("  Running A* search ...");
        String solution = aStarSearch(start, dest);
        if (verbose) println("  Finished A* search.");
        println("Solved! (" + ((System.nanoTime() - startTime) / 1000000L) + " ms)");
        return solution;
    }

    /**
     * A* search algorithm returns the optimal valid route from the given start configuration to the given destination
     * configuration.
     * @param start Start configuration String
     * @param dest Destination configuration String
     * @return Solution route given as concatenated configuration Strings
     */
    private static String aStarSearch(String start, String dest)
    {
        // NOTE: method based on code shown in lectures by Andy King: https://moodle.kent.ac.uk/2016/pluginfile.php/956/course/section/58882/co884-lecture3-handouts.pdf

        PriorityQueue<Config> configQueue = new PriorityQueue<Config>();
        configQueue.add(new Config(diff(start, dest), start));

        while (true)
        {
            if (configQueue.size() == 0) return null;

            LinkedList<String> route = configQueue.poll().getRoute();

            String last = route.getLast();
            if (last.equals(dest)) return concatList(route);

            String[] nextConfigs = getNextConfigs(last);
            for (String next : nextConfigs)
            {
                if (next == null || route.contains(next)) continue;

                LinkedList<String> nextRoute = new LinkedList<String>(route);
                nextRoute.addLast(next);
                int diff = nextRoute.size() + diff(next, dest);
                configQueue.add(new Config(diff, nextRoute));
            }
        }
    }

    /**
     * For the given configuration, returns all the possible configurations after one move.
     * A.K.A. "next_configs"
     * @param config Configuration String
     * @return Next configurations given as array of configuration Strings
     */
    private static String[] getNextConfigs(String config)
    {
        int blank = config.indexOf(CONFIG_BLANK_CHAR);
        Direction[] directions = Direction.values();

        String[] nextConfigs = new String[directions.length];
        for (int i = 0; i < nextConfigs.length; i++)
        {
            if ((directions[i] == Direction.RIGHT && blank % CONFIG_WIDTH == CONFIG_WIDTH - 1)
                    || (directions[i] == Direction.LEFT && blank % CONFIG_WIDTH == 0)) {
                nextConfigs[i] = null;
            } else {
                int move;
                switch (directions[i]) {
                    case UP: move = blank - CONFIG_WIDTH; break;
                    case DOWN: move = blank + CONFIG_WIDTH; break;
                    case LEFT: move = blank - 1; break;
                    case RIGHT: move = blank + 1; break;
                    default: move = -1;
                }

                if (move < 0 || move >= config.length() || config.charAt(move) == CONFIG_IMMOVABLE_CHAR) {
                    nextConfigs[i] = null;
                } else nextConfigs[i] = swapChars(config, blank, move);
            }
        }

        return nextConfigs;
    }

    /**
     * Returns how many chars are different in the given Strings.
     * If the Strings have different length, this difference is added onto the result.
     * A.K.A. tiles out-of-place (tilesOutOfPlace)
     * @param a String
     * @param b String
     * @return Number of chars different
     */
    static int diff(String a, String b)
    {
        int diff = a.length() - b.length();
        if (diff < 0) diff *= -1;

        int length = a.length() > b.length() ? b.length() : a.length();
        for (int i = 0; i < length; i++)
            if (a.charAt(i) != b.charAt(i))
                diff++;

        return diff;
    }
}
